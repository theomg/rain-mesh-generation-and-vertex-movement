﻿using System;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif
using System.Collections.Generic;
using Random = UnityEngine.Random;

public class RainMeshGen : MonoBehaviour
{
#if UNITY_EDITOR
    public int triangleQty = 70000;
    public float maxRadius = 80f;
    public Vector2 minMaxTriWidth = new Vector2(0.004f, 0.025f);
    public Vector2 minMaxTriHeight = new Vector2(2f, 4f);
    public float rotationVariation = 30f;
    public AnimationCurve distribuition = AnimationCurve.Linear(0, 0, 1, 1);
    public GameObject rainParent;
    public bool askPath = true;

    public void GenMesh()
	{
        if (rainParent == null)
        {
            Debug.LogError("No Rain Parent assigned.");
            return;
        }
        if (EditorApplication.isCompiling)
        {
            Debug.LogError("Can't build mesh while compiling. Try again.");
            return;
        }
        var combineInstances1 = new List<CombineInstance>();
        var combineInstances2 = new List<CombineInstance>();
        var combineInstances3 = new List<CombineInstance>();
        var combineInstances4 = new List<CombineInstance>();

        for (int i = 0; i < triangleQty; i++)
        {
            var radius = Mathf.Sqrt(distribuition.Evaluate(Random.Range(0, 1f))) * maxRadius;
            var dir = new Vector2(Random.Range(-1f, 1f), Random.Range(-1f, 1f));
            dir.Normalize();
            var halfWidth = Mathf.Lerp(minMaxTriWidth.x, minMaxTriWidth.y, radius/maxRadius);
            var height = Mathf.Lerp(minMaxTriHeight.x, minMaxTriHeight.y, radius/maxRadius);
            var newMesh = new Mesh();
            newMesh.vertices = new[] { 
                new Vector3(-halfWidth, 0, 0),
                new Vector3(0, height, 0),
                new Vector3(halfWidth, 0, 0) };
            newMesh.triangles = new[] { 0, 1, 2 };
            newMesh.normals = new[] { 
                new Vector3(-.5f, 1, 0).normalized,
                new Vector3(0, 1, 0).normalized,
                new Vector3(.5f, 1, 0).normalized };

            var combine = new CombineInstance();
            combine.mesh = newMesh;
            combine.transform = Matrix4x4.TRS(new Vector3(dir.x * radius, 0, dir.y*radius),
                                                Quaternion.LookRotation(new Vector3(dir.x, 0, dir.y), Vector3.up) * Quaternion.Euler(new Vector3(0, Random.Range(-rotationVariation, rotationVariation), 0)), 
                                                gameObject.transform.localScale);
            combine.subMeshIndex = 0;

            if(dir.x >= 0 && dir.y >= 0)
                combineInstances1.Add(combine);
       else if(dir.x > 0 && dir.y < 0)
                combineInstances2.Add(combine);
       else if (dir.x < 0 && dir.y < 0)
                combineInstances3.Add(combine);
       else if (dir.x < 0 && dir.y > 0)
                combineInstances4.Add(combine);
        }

        var meshes = new List<Mesh> {new Mesh(), new Mesh(), new Mesh(), new Mesh()};
        meshes[0].CombineMeshes(combineInstances1.ToArray());
        meshes[1].CombineMeshes(combineInstances2.ToArray());
        meshes[2].CombineMeshes(combineInstances3.ToArray());
        meshes[3].CombineMeshes(combineInstances4.ToArray());

        for (var j = 0; j < 4; j++)
        {
            var colors = new List<Color>();
            for (var i = 0; i < meshes[j].vertexCount; i += 3)
            {
                var rnd = Random.Range(.4f, 1f);
                colors.Add(new Color(rnd, 1, 1, 1));
                colors.Add(new Color(rnd, 1, 1, 1));
                colors.Add(new Color(rnd, 1, 1, 1));
            }
            meshes[j].colors = colors.ToArray();
        }

        string savePath;
        if (askPath)
            savePath = EditorUtility.SaveFilePanel("Save generated mesh", "Assets/", "rainmesh", "asset");
        else
            savePath = Application.dataPath + "/rainmesh.asset";
        savePath = FileUtil.GetProjectRelativePath(savePath);
        AssetDatabase.CreateAsset(meshes[0], savePath.Insert(savePath.Length - 6, "1"));
        AssetDatabase.CreateAsset(meshes[1], savePath.Insert(savePath.Length - 6, "2"));
        AssetDatabase.CreateAsset(meshes[2], savePath.Insert(savePath.Length - 6, "3"));
        AssetDatabase.CreateAsset(meshes[3], savePath.Insert(savePath.Length - 6, "4"));
        AssetDatabase.SaveAssets();

        var meshCounter = 0;
        foreach (var meshfilter in rainParent.GetComponentsInChildren<MeshFilter>())
        {
            meshfilter.mesh = meshes[meshCounter++];
        }
    }
#endif
}



#if UNITY_EDITOR
[CustomEditor(typeof(RainMeshGen))]
public class RainMeshGenEditor : Editor
{
    public override void OnInspectorGUI()
    {
        var rainMeshGen = (RainMeshGen)target;
        base.OnInspectorGUI();
        if (GUILayout.Button("Generate Rain Mesh"))
        {
            rainMeshGen.GenMesh();
        }
    }
}
#endif