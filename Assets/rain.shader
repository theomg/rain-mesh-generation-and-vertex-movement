﻿Shader "Custom/rain" {
	Properties{
		_Color("Color", Color) = (1,1,1,1)
		_Emissive("Emissive", Color) = (1,1,1,1)
		_Strength("Strength", Range(0,1)) = 1.0
		_Speed("Speed", Float) = 160.0
		_MinHeight("Min Height", Float) = -100.0
		_MaxHeight("Max Height", Float) = 100.0
	}
		SubShader{
		Tags{ "RenderType" = "Opaque"}
		
		CGPROGRAM

		#pragma surface surf Standard vertex:vert
		#pragma target 3.0


		sampler2D _MainTex;

			struct Input {
				fixed4 color : COLOR;
			};

			struct appdata_custom {
				float4 vertex : POSITION;
				float4 tangent : TANGENT;
				float3 normal : NORMAL;
				float4 texcoord : TEXCOORD0;
				float4 texcoord1 : TEXCOORD1;
				float4 texcoord2 : TEXCOORD2;
				float4 texcoord3 : TEXCOORD3;
		#if defined(SHADER_API_XBOX360)
				half4 texcoord4 : TEXCOORD4;
				half4 texcoord5 : TEXCOORD5;
		#endif
				fixed4 color : COLOR;
				uint   id    : SV_VertexID;
				UNITY_INSTANCE_ID
			};

			fixed4 _Color;
			half3 _Emissive;
			float _Strength;
			half _Speed;
			half _MinHeight;
			half _MaxHeight;

			void vert(inout appdata_custom v, out Input o)
			{
				UNITY_INITIALIZE_OUTPUT(Input, o);
				float id = floor(v.id / 3);
				float4 pos = v.vertex;
				float time = fmod(_Time.y * _Speed * v.color.r + 30, 60)/60;
				pos.y += lerp(_MaxHeight, _MinHeight, time);
				float kill = max(v.color.r - (1 - _Strength), 0);
				pos = (kill == 0) ? 0 : pos;
				v.vertex = pos;
			}

			void surf(Input IN, inout SurfaceOutputStandard o) {
				fixed4 c = _Color;
				o.Albedo = c.rgb;
				o.Metallic = 0;
				o.Smoothness = 0;
				o.Alpha = c.a;
				o.Emission = _Emissive;
			}
			ENDCG
	}
	FallBack "Diffuse"
}
